# Study of external memory algorithms in Java
The purpose of this project is to benchmark several methods for reading and writing in secondary memory. The ones that performed best were then used to implement an external memory merge-sort algorithm. All of the files used during the experiments are text files from the IMDB dataset which is a snapshot of data harvested from the IMDB movie database. 
 
The language used for this project was Java. All measurements done during the experiments were acquired with the JMH (Java Microbenchmark Harness) library and the Intellij IDEA.The Maven plug-in is also required to run JMH. It is thus important to note that the part of the code used to benchmark cannot work without using the IntelliJ IDEA.

More details about the project can be found in *database.pdf*.
