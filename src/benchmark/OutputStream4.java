package benchmark;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class OutputStream4 extends OutputStream {
    private MappedByteBuffer b = null;
    private FileChannel channel =null;
    private RandomAccessFile file = null;
    private int offset = 0;
    private int size = 4096;
    private int data_offset;

    public OutputStream4(String filePath) {
        super(filePath);
        this.open(filePath);
    }

    public OutputStream4(String filePath, int size) {
        super(filePath);
        this.size = size;
        this.open(filePath);
    }
    @Override
    public void writeln(String data){
        data+="\n";
        data_offset = 0;
        try {
            while(data_offset < data.length()){
                if(data.length() <= size-offset) {
                    b.put(data.getBytes(),data_offset, data.length() - data_offset);
                    offset += data.length();
                    data_offset += data.length();
                }
                else{

                    b.put(data.getBytes(),data_offset, size-offset);
                    data_offset+=size-offset;
                    b = channel.map(FileChannel.MapMode.READ_WRITE, file.length(), size);
                    offset = 0;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void open(String filePath) {
        try {
            file = new RandomAccessFile(new File(filePath), "rw");
            channel = file.getChannel();
            b = channel.map(FileChannel.MapMode.READ_WRITE, file.length(), size);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        try {
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
