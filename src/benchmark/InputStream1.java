package benchmark;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class InputStream1 extends InputStream {
    private FileReader is = null;

    public InputStream1(String filePath) {
        super(filePath);
        open();
    }

    @Override
    public int readln(StringBuilder sb) {
        int len = 0;
        try {
            int c = is.read(); //read a single character
            if (c == -1) {
                return -1;
            }
            while (c != -1 && c !='\n' && c != '\r'){//read until the end of the of the file or the end of the line
                sb.append((char) c);
//                System.out.print((char) c);//print every read character
                c = is.read();
                len += 1; //increment length of the line
            }
            if (c != -1) {
//                System.out.print((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return len; //return length of the line
    }

    @Override
    public int randjump(int j) {
        StringBuilder sb = new StringBuilder();
        if (j < 0) {
            System.out.println("Error : j must be a positive integer");
            return -1;
        }
        int sum = 0, l;
        long length = 0;
        try {
            length = file.length();
        } catch (IOException e) {
            e.printStackTrace();
        }

        long p;
        for (int i = 0; i < j; i++) {
            p = randomizer.nextInt((int) length);
            seek(p);
            l = readln(sb);
            rewind();
            sum += l;
        }
        return sum;
    }

    @Override
    public void seek(long pos) {
        try {
            is.skip(pos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void open() {
        super.open();
        try {
            is = new FileReader( new File(filePath) );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        try {
            super.close();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void rewind(){
        try {
            is = new FileReader( new File(filePath) );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
