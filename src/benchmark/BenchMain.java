package benchmark;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Fork(value = 10, jvmArgs = {"-Xms2G", "-Xmx2G"})
@Warmup(iterations = 5)
@Measurement(iterations = 15)
public class BenchMain {
    @State(Scope.Benchmark)
    public static class BenchState {
        public int j;
       // @Param({"D:\\database\\unzipped\\comp_cast_type.csv", "D:\\database\\unzipped\\info_type.csv", "D:\\database\\unzipped\\movie_link.csv", "D:\\database\\unzipped\\keyword.csv", "D:\\database\\unzipped\\movie_info_idx.csv", "D:\\database\\unzipped\\aka_name.csv", "D:\\database\\unzipped\\name.csv", "D:\\database\\unzipped\\cast_info.csv"})
        @Param({"D:\\database\\unzipped\\cast_info.csv"})
        public String filepath;

        //@Param({"1", "10", "100", "1000", "3000", "5000", "10000", "100000"})
        @Param({"4096"})
        public int size;

        //@Param({"10","1"})
        //public int mult;

        @Setup(Level.Trial)
        public void sep() {
            /*
            switch (filepath) {
                case "D:\\database\\unzipped\\comp_cast_type.csv" :
                    j = 1;
                    // 45o - 4ko sur page
                    // 5 lignes
                    break;
                case "D:\\database\\unzipped\\info_type.csv" :
                    j = 1;
                    // 85o - 4ko sur page
                    // 8 lignes
                    break;
                case "D:\\database\\unzipped\\movie_link.csv" :
                    j = (29998 / (mult * 4096));
                    if(j == 0) j=1;
                    // j = (lignes / (10 * pagesize)) et (lignes / pagesize)
                    //    j = (lignes / (mult * pagesize)
                    // 657ko - 659ko sur page
                    // 29998 lignes
                    break;
                case "D:\\database\\unzipped\\keyword.csv" :
                    j = (134171 / (mult * 4096));
                    // 3,8Mo - 3,8Mo sur page
                    // 134171 lignes
                    break;
                case "D:\\database\\unzipped\\movie_info_idx.csv" :
                    j = (1380036 / (mult * 4096));
                    // 35,3Mo - 35,3 Mo sur page
                    // 1.380.036 lignes
                    break;
                case "D:\\database\\unzipped\\aka_name.csv" :
                    j = (901344 / (mult * 4096));
                    // 73Mo - 73Mo sur disque
                    // 901344 lignes
                    break;
                case "D:\\database\\unzipped\\name.csv" :
                    j = (1167492 / (mult * 4096));
                    // 321,2Mo - 322,3Mo sur disque
                    // 1.167.492 lignes
                    break;
                case "D:\\database\\unzipped\\cast_info.csv" :
                    j = (36244345 / (mult * 4096));
                    // 1,42Go - 1,42Go
                    // 36.244.345 lignes
                    break;
            }*/
            System.out.println("\n<<<<< >>>>>");
            System.out.println("filepath : " + filepath);
            System.out.println("size buffer : " + size);
            //System.out.println("hop count j : " + j);
            //System.out.println("mult : " + mult);
            System.out.println("<<<<< >>>>>");
        }
    }


    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(BenchMain.class.getSimpleName())
                .forks(1)
                .build();
        new Runner(opt).run();
    }

    @org.openjdk.jmh.annotations.Benchmark
    public void benchmarker(BenchState bs, Blackhole blackhole){
        InputStream is = new InputStream4(bs.filepath,1000000000);
        blackhole.consume(is.length());
        is.close();
    }


}
