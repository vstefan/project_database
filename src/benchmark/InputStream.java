package benchmark;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;

public  abstract class InputStream {
    protected final String filePath;
    protected RandomAccessFile file = null;
    protected final Random randomizer = new Random(1387479);

    protected InputStream(String filePath) {
        this.filePath = filePath;
    }

    public abstract int readln(StringBuilder sb);

    public void open(){
        try {
            file = new RandomAccessFile(new File(filePath), "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void close(){
        try {
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public abstract int randjump(int j);
    public abstract void seek(long pos);

    public int length(){
        StringBuilder s = new StringBuilder();
        int sum = 0;//length of the file
        int a = this.readln(s);
        while(a != -1) {//read until we reach the end of the file
            sum += a;//add length of the line to length of the file
            a = this.readln(s);
        }
        this.close();
        return sum;//return length of the file
    }
}
