package benchmark;

import java.util.Comparator;

public class SortByK implements Comparator<String[]> {
    private int k;

    public SortByK(int k){
        this.k = k;
    }

    @Override
    public int compare(String[] o1, String[] o2) {
        return o1[k].compareTo(o2[k]);
    }
}
