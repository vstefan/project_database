package benchmark;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class OutputStream2 extends OutputStream {
    private FileWriter os = null;
    private BufferedWriter bw2 = null;
    private int buffSize = 1;

    public OutputStream2(String filePath) {
        super(filePath);
        this.open(filePath);
    }

    public OutputStream2(String filePath, int buffSize){
        super(filePath);
        this.buffSize = buffSize;
    }

    @Override
    public void writeln(String data){
        try {
            bw2.write(data+'\n');
            bw2.flush();
        } catch (IOException e) {e.printStackTrace();}
    }

    @Override
    protected void open(String filePath) {
        try {
            os = new FileWriter( new File(filePath) );
            bw2 = new BufferedWriter(os);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void close() {
        try {
            bw2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
