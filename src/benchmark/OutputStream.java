package benchmark;

import java.io.File;
import java.io.IOException;

public abstract class OutputStream {
    private String filePath;

    public OutputStream(String filePath) {
        this.filePath = filePath;
        this.create();
    }

    public abstract void writeln(String data);

    protected void create(){
        try {
            new File(filePath).createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected abstract void open(String filePath);

    public abstract void close();
}
