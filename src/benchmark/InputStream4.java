package benchmark;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class InputStream4 extends InputStream {
    private MappedByteBuffer b = null;
    private FileChannel channel = null;
    private int buffSize = 10;
    private int offset = 0;
    private int startPos = 0;

    public InputStream4(String filePath) {
        super(filePath);
        open();
    }
    public InputStream4(String filePath, int buffSize) {
        super(filePath);
        this.buffSize = buffSize;
        open();
    }


    @Override
    public void close(){
        super.close();
        b= null;
    }


    public int length2(){
        int len = 0;
        try {
            b = channel.map(FileChannel.MapMode.READ_ONLY, 0, file.length());
            while (b.hasRemaining()){
                char c = (char) b.get();
                len++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return len;
    }

    @Override
    public int readln(StringBuilder s) {
        int len = 0;
        while (true){
            try {
                if (offset == file.length()) {
                    if (len == 0) {
                        return -1;
                    } else {
                        return len;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //System.out.println("Not going in the if " + (offset - startPos));
            if (offset - startPos < 0 || offset-startPos >= b.limit()) {
                try {
                    long size = Math.min(buffSize, file.length() - offset);
                    //System.out.println("This is a mapping");
                    b = channel.map(FileChannel.MapMode.READ_ONLY, offset, size);
                    startPos = offset;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            for (int i = offset - startPos; i < b.limit(); i++) {
                char c = (char) b.get(i);
                s.append(c);
                //System.out.print(c);
                len++;
                offset++;
                if (c == '\n' || c == '\r') {
                    //offset += i+1;
                    return len;
                }
            }
        }

    }
    @Override
    public void open(){
        super.open();
        channel = file.getChannel();
        try {
            long size = Math.min(buffSize, file.length() - offset);
            b = channel.map(FileChannel.MapMode.READ_ONLY, 0, size);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int randjump(int j) {
        StringBuilder sb = new StringBuilder();
        if (j < 0) {
            System.out.println("Error : j must be a positive integer");
            return -1;
        }
        int sum = 0, l;
        long length = 0;
        try {
            length = file.length();
        } catch (IOException e) {
            e.printStackTrace();
        }

        long p;
        for (int i = 0; i < j; i++) {
            p = randomizer.nextInt((int) length);
            seek(p);
            l = readln(sb);
            sum += l;
        }
        return sum;
    }

    @Override
    public void seek(long pos)  {
        offset = (int)pos;
    }


}
