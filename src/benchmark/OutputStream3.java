package benchmark;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class OutputStream3 extends OutputStream {
    private int buffSize = 10;
    private FileWriter os = null;
    private BufferedWriter bw3 = null;

    public OutputStream3(String filePath) {
        super(filePath);
        this.open(filePath);
    }

    public OutputStream3(String filePath, int buffSize){
        super(filePath);
        this.buffSize = buffSize;
        this.open(filePath);
    }

    @Override
    public void writeln(String data){
        try {
            bw3.write(data+'\n');
            bw3.flush();
        } catch (IOException e) {e.printStackTrace();}
    }

    @Override
    protected void open(String filePath) {
        try {
            os = new FileWriter( new File(filePath) );
            bw3 = new BufferedWriter(os,buffSize);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void close() {
        try {
            bw3.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
