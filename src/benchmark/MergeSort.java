package benchmark;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class MergeSort {
    private String filePath;
    private File file;
    private int M;
    private int d;
    private int k;
    private int cnt;
    private ArrayList<String> files = new ArrayList<>();


    public MergeSort(String filePath,int M, int d, int k){
        this.filePath = filePath;
        this.M = M;
        this.d = d;
        this.k = k;

        file = new File(filePath);
    }

    public void splitFile(InputStream is){
        ArrayList<String[]> buffer;
        int len;
        String overflow = null;
        StringBuilder sb = new StringBuilder();
        int it = (int) (file.length()/M);
        for(int i = 0; i < it ; i++){ //TODO the number of iterations is not correct
            String f = "test\\file"+cnt+".txt";
            cnt++;
            files.add(f);
            OutputStream os = new OutputStream1(f); //TODO put correct implementation
            buffer = new ArrayList<>();
            len = 0;
            String line;
            if (overflow != null) {buffer.add(overflow.split(" ")); overflow = null;}
            while(true){
                len += is.readln(sb); //TODO solve problem when readln returns -1
                line = sb.toString();
                sb.setLength(0);
                System.out.println(len);
                if (line.length() < M - len){ buffer.add(line.split(" "));}
                else{ overflow = line;break;}
            }
            System.out.println(buffer.size());
            Collections.sort(buffer,new SortByK(k));
            //System.out.println("Everything is sorted------------------------------------------------");
            int size = buffer.size();
            for(int j = 0; j < size; j++){
                StringBuilder builder = new StringBuilder();
                for(String s : buffer.get(j)) {
                    builder.append(s);
                }
                String str = builder.toString();
                os.writeln(str);
            }
            os.close();
        }
    }

    public void splitFile2(InputStream is){
        ArrayList<String[]> buffer;
        int len;
        String overflow = null;
        StringBuilder sb = new StringBuilder();


        int it = (int) (file.length()/M);
        for(int i = 0; i < it ; i++){ //TODO the number of iterations is not correct
            String f = "test\\file"+cnt+".txt";
            cnt++;
            files.add(f);
            OutputStream os = new OutputStream1(f); //TODO put correct implementation
            buffer = new ArrayList<>();
            len = 0;
            String line;
            if (overflow != null) {buffer.add(overflow.split(" ")); overflow = null;}
            while(true){
                len += is.readln(sb); //TODO solve problem when readln returns -1
                line = sb.toString();
                sb.setLength(0);
                System.out.println(len);
                if (line.length() < M - len){ buffer.add(line.split(" "));}
                else{ overflow = line;break;}
            }
            System.out.println(buffer.size());
            Collections.sort(buffer,new SortByK(k));
            //System.out.println("Everything is sorted------------------------------------------------");
            int size = buffer.size();
            for(int j = 0; j < size; j++){
                StringBuilder builder = new StringBuilder();
                for(String s : buffer.get(j)) {
                    builder.append(s);
                }
                String str = builder.toString();
                os.writeln(str);
            }
            os.close();
        }
    }
}
