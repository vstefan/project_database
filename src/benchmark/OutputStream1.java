package benchmark;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class OutputStream1 extends OutputStream {

    private FileWriter os = null;

    public OutputStream1(String filePath) {
        super(filePath);
        this.open(filePath);
    }

    @Override
    public void writeln(String data){
        int len = 0;
        try {
            for(int i = 0; i < data.length(); i++){
                os.write(data.charAt(i)); //write a single character
            }
            os.write('\n'); //write a single character
        } catch (IOException e) {e.printStackTrace();}
    }


    @Override
    protected void open(String filePath){
        try {
            os = new FileWriter( new File(filePath) );
        } catch (IOException e) {e.printStackTrace();}
    }

    @Override
    public void close(){
        try {
            os.close();
        } catch (IOException e) {e.printStackTrace();}
    }
}