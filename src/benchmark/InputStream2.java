package benchmark;

import java.io.*;

public class InputStream2 extends InputStream {
    private FileReader is = null;
    private BufferedReader br2 = null;

    public InputStream2(String filePath) {
        super(filePath);
        open();

    }

    @Override
    public int readln(StringBuilder s) {
        int len = 0;
        try {
            String ret = br2.readLine();//read a line
            if (ret != null){//If we didn't reach EOF, print result and increment length
                s.append(ret);
                //System.out.println(ret);
                len = ret.length();
            }
            else return -1; // we return -1 because of the EOF
        } catch (IOException e) {
            e.printStackTrace();
        }
        return len;//return length of the line
    }

    @Override
    public void open() {
        try {
            super.open();
            is = new FileReader( new File(filePath) );
            br2 = new BufferedReader( is );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        try {
            super.close();
            is.close();
            br2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int randjump(int j) {
        StringBuilder sb = new StringBuilder();
        if (j < 0) {
            System.out.println("Error : j must be a positive integer");
            return -1;
        }
        int sum = 0, l;
        long length = 0;
        try {
            length = file.length();
        } catch (IOException e) {
            e.printStackTrace();
        }

        long p;
        for (int i = 0; i < j; i++) {
            p = randomizer.nextInt((int) length);
            seek(p);
            l = readln(sb);
            rewind();
            sum += l;
        }
        return sum;
    }

    @Override
    public void seek(long pos) {
        try {
            br2.skip(pos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void rewind(){
        try {
            is = new FileReader( new File(filePath) );
            br2 = new BufferedReader( is );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
