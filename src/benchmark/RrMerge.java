package benchmark;

import java.util.ArrayList;
import java.util.List;

public class RrMerge {
    private OutputStream os;
    private List<InputStream> iss = new ArrayList<>();

    public RrMerge(OutputStream os,Boolean is, String... files){
        this.os = os;
        if (is) {
            for (String file : files) {
                iss.add(new InputStream2(file));
            }
        }
        else {
            for (String file : files) {
                iss.add(new InputStream4(file, 4096));
            }
        }
    }

    public void merge(){
        StringBuilder s = new StringBuilder();;
        while (!iss.isEmpty()){
            for (int i = 0; i < iss.size();i++){

                if (iss.get(i).readln(s) == -1){
                    iss.get(i).close();
                    iss.remove(iss.get(i));
                }
                else {
                    os.writeln(s.toString());
                    s.setLength(0);
                }
            }
        }
        os.close();
    }
}
