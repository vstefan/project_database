import benchmark.*;

import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        OutputStream os = new OutputStream4("merge_test3.txt",4096);
        RrMerge m = new RrMerge(os, true,"merged1.txt","cleanertest.txt","merged2.txt");
        m.merge();
    }
}
